<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOQueOferecemosTable extends Migration
{
    public function up()
    {
        Schema::create('o_que_oferecemos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('texto');
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('o_que_oferecemos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quadro_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('quadro_id')->references('id')->on('o_que_oferecemos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('o_que_oferecemos_imagens');
        Schema::drop('o_que_oferecemos');
    }
}
