<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chamada_1_icone');
            $table->text('chamada_1_titulo');
            $table->text('chamada_1_texto');
            $table->string('chamada_2_icone');
            $table->text('chamada_2_titulo');
            $table->text('chamada_2_texto');
            $table->string('chamada_3_icone');
            $table->text('chamada_3_titulo');
            $table->text('chamada_3_texto');
            $table->string('abertura_titulo');
            $table->text('abertura_texto');
            $table->string('codigo_video');
            $table->text('creditos_video');
            $table->text('texto_secundario');
            $table->string('imagem_lateral');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
