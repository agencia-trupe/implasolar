<?php

use Illuminate\Database\Seeder;

class ServicosSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'imagem' => '',
            'texto_principal' => '',
            'texto_secundario' => '',
            'rodape' => '',
        ]);
    }
}
