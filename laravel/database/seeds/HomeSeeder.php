<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'chamada_1_icone' => '',
            'chamada_1_titulo' => '',
            'chamada_1_texto' => '',
            'chamada_2_icone' => '',
            'chamada_2_titulo' => '',
            'chamada_2_texto' => '',
            'chamada_3_icone' => '',
            'chamada_3_titulo' => '',
            'chamada_3_texto' => '',
            'abertura_titulo' => '',
            'abertura_texto' => '',
            'codigo_video' => '',
            'creditos_video' => '',
            'texto_secundario' => '',
            'imagem_lateral' => '',
        ]);
    }
}
