<?php

return [

    'name'        => 'Implasolar',
    'title'       => 'Implasolar',
    'description' => 'Reduza seus gastos com energia elétrica com placas solares. Gere eletricidade na sua própria casa ou empresa.',
    'keywords'    => 'painéis solares, energia solar, painéis fotovoltaicos, instalação de painéis solares, instalação de painéis fotovoltaicos, venda de painéis solares, venda de paineis fotovoltaicos, paineis solares, energia solar, economia com eneria solar',
    'shareImg'    => '',
    'analytics'   => null

];
