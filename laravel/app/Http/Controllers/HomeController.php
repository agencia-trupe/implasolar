<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Home;
use App\Models\Utilizacao;

class HomeController extends Controller
{
    public function index()
    {
        $banners     = Banner::ordenados()->get();
        $home        = Home::first();
        $utilizacoes = Utilizacao::ordenados()->get();

        return view('frontend.home', compact('banners', 'home', 'utilizacoes'));
    }
}
