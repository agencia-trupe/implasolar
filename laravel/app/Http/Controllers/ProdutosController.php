<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\OrcamentosProdutosRequest;

use App\Models\Produto;
use App\Models\Produtostexto;
use App\Models\Contato;
use App\Models\OrcamentoProdutos;

class ProdutosController extends Controller
{
    public function index()
    {
        $produtos = Produto::ordenados()->get();
        $texto    = Produtostexto::first();

        return view('frontend.produtos', compact('produtos', 'texto'));
    }

    public function post(OrcamentosProdutosRequest $request, OrcamentoProdutos $orcamento)
    {
        $orcamento->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.orcamento-produtos', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[ORÇAMENTO DE PRODUTOS] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('enviado', true);
    }
}
