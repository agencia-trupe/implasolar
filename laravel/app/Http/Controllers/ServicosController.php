<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\OrcamentosServicosRequest;

use App\Models\Servicos;
use App\Models\Financiamento;
use App\Models\Contato;
use App\Models\OrcamentoServicos;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos       = Servicos::first();
        $financiamentos = Financiamento::ordenados()->get();

        return view('frontend.servicos', compact('servicos', 'financiamentos'));
    }

    public function post(OrcamentosServicosRequest $request, OrcamentoServicos $orcamento)
    {
        $contato = Contato::first();

        $input = $request->all();
        if ($request->hasFile('conta_de_luz')) {
            $input['conta_de_luz'] = OrcamentoServicos::upload_conta();
        }

        $orcamento->create($input);

        if (isset($contato->email)) {
            \Mail::send('emails.orcamento-servicos', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[ORÇAMENTO DE SERVIÇOS] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('enviado', true);
    }
}
