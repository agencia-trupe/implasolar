<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\UtilizacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Utilizacao;

class UtilizacoesController extends Controller
{
    public function index()
    {
        $registros = Utilizacao::ordenados()->get();

        return view('painel.utilizacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.utilizacoes.create');
    }

    public function store(UtilizacoesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Utilizacao::upload_imagem();

            Utilizacao::create($input);

            return redirect()->route('painel.utilizacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Utilizacao $registro)
    {
        return view('painel.utilizacoes.edit', compact('registro'));
    }

    public function update(UtilizacoesRequest $request, Utilizacao $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Utilizacao::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.utilizacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Utilizacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.utilizacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
