<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutostextoRequest;
use App\Http\Controllers\Controller;

use App\Models\Produtostexto;

class ProdutostextoController extends Controller
{
    public function index()
    {
        $registro = Produtostexto::first();

        return view('painel.produtostexto.edit', compact('registro'));
    }

    public function update(ProdutostextoRequest $request, Produtostexto $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.produtostexto.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
