<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HomeRequest;
use App\Http\Controllers\Controller;

use App\Models\Home;

class HomeController extends Controller
{
    public function index()
    {
        $registro = Home::first();

        return view('painel.home.edit', compact('registro'));
    }

    public function update(HomeRequest $request, Home $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['chamada_1_icone'])) $input['chamada_1_icone'] = Home::upload_chamada_1_icone();
            if (isset($input['chamada_2_icone'])) $input['chamada_2_icone'] = Home::upload_chamada_2_icone();
            if (isset($input['chamada_3_icone'])) $input['chamada_3_icone'] = Home::upload_chamada_3_icone();
            if (isset($input['imagem_lateral'])) $input['imagem_lateral'] = Home::upload_imagem_lateral();

            $registro->update($input);

            return redirect()->route('painel.home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
