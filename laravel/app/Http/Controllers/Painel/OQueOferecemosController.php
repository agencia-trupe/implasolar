<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OQueOferecemosRequest;
use App\Http\Controllers\Controller;

use App\Models\Quadro;

class OQueOferecemosController extends Controller
{
    public function index()
    {
        $registros = Quadro::ordenados()->get();

        return view('painel.o-que-oferecemos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.o-que-oferecemos.create');
    }

    public function store(OQueOferecemosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Quadro::upload_imagem();

            Quadro::create($input);

            return redirect()->route('painel.o-que-oferecemos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Quadro $registro)
    {
        return view('painel.o-que-oferecemos.edit', compact('registro'));
    }

    public function update(OQueOferecemosRequest $request, Quadro $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Quadro::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.o-que-oferecemos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Quadro $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.o-que-oferecemos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
