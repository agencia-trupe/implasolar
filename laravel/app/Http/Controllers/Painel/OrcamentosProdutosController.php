<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OrcamentoProdutos;

class OrcamentosProdutosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = OrcamentoProdutos::orderBy('created_at', 'DESC')->get();

        return view('painel.orcamentos-produtos.index', compact('contatosrecebidos'));
    }

    public function show(OrcamentoProdutos $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.orcamentos-produtos.show', compact('contato'));
    }

    public function destroy(OrcamentoProdutos $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.orcamentos-produtos.index')->with('success', 'Orçamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir orçamento: '.$e->getMessage()]);

        }
    }

    public function toggle(OrcamentoProdutos $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.orcamentos-produtos.index')->with('success', 'Orçamento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar orçamento: '.$e->getMessage()]);

        }
    }
}
