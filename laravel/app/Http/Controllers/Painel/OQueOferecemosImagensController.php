<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OQueOferecemosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Quadro;
use App\Models\QuadroImagem;

use App\Helpers\CropImage;

class OQueOferecemosImagensController extends Controller
{
    public function index(Quadro $registro)
    {
        $imagens = QuadroImagem::quadro($registro->id)->ordenados()->get();

        return view('painel.o-que-oferecemos.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Quadro $registro, QuadroImagem $imagem)
    {
        return $imagem;
    }

    public function store(Quadro $registro, OQueOferecemosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = QuadroImagem::uploadImagem();
            $input['quadro_id'] = $registro->id;

            $imagem = QuadroImagem::create($input);

            $view = view('painel.o-que-oferecemos.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Quadro $registro, QuadroImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.o-que-oferecemos.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Quadro $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.o-que-oferecemos.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
