<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OrcamentoServicos;

class OrcamentosServicosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = OrcamentoServicos::orderBy('created_at', 'DESC')->get();

        return view('painel.orcamentos-servicos.index', compact('contatosrecebidos'));
    }

    public function show(OrcamentoServicos $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.orcamentos-servicos.show', compact('contato'));
    }

    public function destroy(OrcamentoServicos $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.orcamentos-servicos.index')->with('success', 'Orçamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir orçamento: '.$e->getMessage()]);

        }
    }

    public function toggle(OrcamentoServicos $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.orcamentos-servicos.index')->with('success', 'Orçamento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar orçamento: '.$e->getMessage()]);

        }
    }
}
