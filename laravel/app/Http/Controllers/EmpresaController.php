<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Empresa;
use App\Models\Quadro;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();
        $quadros = Quadro::ordenados()->get();

        return view('frontend.empresa', compact('empresa', 'quadros'));
    }
}
