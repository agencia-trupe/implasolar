<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OQueOferecemosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'texto' => 'required',
            'imagem' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
