<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrcamentosServicosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'         => 'required',
            'email'        => 'required|email',
            'conta_de_luz' => 'mimes:pdf,jpeg,png',
            'mensagem'     => 'required'
        ];
    }
}
