<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada_1_icone' => 'image',
            'chamada_1_titulo' => 'required',
            'chamada_1_texto' => 'required',
            'chamada_2_icone' => 'image',
            'chamada_2_titulo' => 'required',
            'chamada_2_texto' => 'required',
            'chamada_3_icone' => 'image',
            'chamada_3_titulo' => 'required',
            'chamada_3_texto' => 'required',
            'abertura_titulo' => 'required',
            'abertura_texto' => 'required',
            'codigo_video' => 'required',
            'creditos_video' => 'required',
            'texto_secundario' => 'required',
            'imagem_lateral' => 'image',
        ];
    }
}
