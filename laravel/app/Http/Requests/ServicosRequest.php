<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'texto_principal' => 'required',
            'texto_secundario' => 'required',
            'rodape' => 'required',
        ];
    }
}
