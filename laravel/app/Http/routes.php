<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('servicos', 'ServicosController@index')->name('servicos');
    Route::post('servicos', 'ServicosController@post')->name('servicos.post');
    Route::get('produtos', 'ProdutosController@index')->name('produtos');
    Route::post('produtos', 'ProdutosController@post')->name('produtos.post');
    Route::get('solicite-um-orcamento', 'ServicosController@index')->name('orcamento');
    Route::get('fale-conosco', 'ContatoController@index')->name('contato');
    Route::post('fale-conosco', 'ContatoController@post')->name('contato.post');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('o-que-oferecemos', 'OQueOferecemosController');
		Route::get('o-que-oferecemos/{o_que_oferecemos}/imagens/clear', [
			'as'   => 'painel.o-que-oferecemos.imagens.clear',
			'uses' => 'OQueOferecemosImagensController@clear'
		]);
		Route::resource('o-que-oferecemos.imagens', 'OQueOferecemosImagensController', ['parameters' => ['imagens' => 'imagens_o-que-oferecemos']]);
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('utilizacoes', 'UtilizacoesController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('servicos', 'ServicosController', ['only' => ['index', 'update']]);
		Route::resource('financiamentos', 'FinanciamentosController');
		Route::resource('produtostexto', 'ProdutostextoController', ['only' => ['index', 'update']]);
		Route::resource('produtos', 'ProdutosController');

        Route::get('orcamentos-produtos/{orcamentos_produtos}/toggle', ['as' => 'painel.orcamentos-produtos.toggle', 'uses' => 'OrcamentosProdutosController@toggle']);
        Route::resource('orcamentos-produtos', 'OrcamentosProdutosController');
        Route::get('orcamentos-servicos/{orcamentos_servicos}/toggle', ['as' => 'painel.orcamentos-servicos.toggle', 'uses' => 'OrcamentosServicosController@toggle']);
        Route::resource('orcamentos-servicos', 'OrcamentosServicosController');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
