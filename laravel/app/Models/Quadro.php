<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Quadro extends Model
{
    protected $table = 'o_que_oferecemos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\QuadroImagem', 'quadro_id')->ordenados();
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 460,
            'height' => null,
            'path'   => 'assets/img/o-que-oferecemos/'
        ]);
    }
}
