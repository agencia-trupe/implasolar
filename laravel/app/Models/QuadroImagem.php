<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QuadroImagem extends Model
{
    protected $table = 'o_que_oferecemos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeQuadro($query, $id)
    {
        return $query->where('quadro_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'bgcolor' => '#fff',
                'path'    => 'assets/img/o-que-oferecemos/imagens/thumbs/'
            ],
            [
                'width'   => null,
                'height'  => 60,
                'path'    => 'assets/img/o-que-oferecemos/imagens/'
            ]
        ]);
    }
}
