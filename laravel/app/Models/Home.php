<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_chamada_1_icone()
    {
        return CropImage::make('chamada_1_icone', [
            'width'  => 100,
            'height' => 100,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_chamada_2_icone()
    {
        return CropImage::make('chamada_2_icone', [
            'width'  => 100,
            'height' => 100,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_chamada_3_icone()
    {
        return CropImage::make('chamada_3_icone', [
            'width'  => 100,
            'height' => 100,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_imagem_lateral()
    {
        return CropImage::make('imagem_lateral', [
            'width'  => 600,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
    }

}
