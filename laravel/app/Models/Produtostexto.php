<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Produtostexto extends Model
{
    protected $table = 'produtostexto';

    protected $guarded = ['id'];

}
