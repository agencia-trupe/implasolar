<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('o-que-oferecemos', 'App\Models\Quadro');
		$router->model('imagens_o-que-oferecemos', 'App\Models\QuadroImagem');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('utilizacoes', 'App\Models\Utilizacao');
		$router->model('home', 'App\Models\Home');
		$router->model('banners', 'App\Models\Banner');
		$router->model('servicos', 'App\Models\Servicos');
		$router->model('financiamentos', 'App\Models\Financiamento');
		$router->model('produtostexto', 'App\Models\Produtostexto');
		$router->model('produtos', 'App\Models\Produto');
        $router->model('orcamentos-produtos', 'App\Models\OrcamentoProdutos');
        $router->model('orcamentos-servicos', 'App\Models\OrcamentoServicos');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
