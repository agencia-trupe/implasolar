(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            speed: 1500,
            pagerTemplate: '<a href=#>{{slideNum}}</a>'
        });
    };

    App.envioContato = function() {
        $('#form-contato').submit(function(event) {
            event.preventDefault();

            var $form     = $(this),
                $response = $('#form-contato-response');

            $response.fadeOut('fast');

            $.ajax({
                type: "POST",
                url: $('base').attr('href') + '/fale-conosco',
                data: {
                    nome: $('#nome').val(),
                    email: $('#email').val(),
                    telefone: $('#telefone').val(),
                    mensagem: $('#mensagem').val(),
                },
                success: function(data) {
                    $response.fadeOut().text(data.message).fadeIn('slow');
                    $form[0].reset();
                },
                error: function(data) {
                    $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
                },
                dataType: 'json'
            });
        });
    };

    App.envioContatoFooter = function() {
        $('#form-contato-footer').submit(function(event) {
            event.preventDefault();

            var $form     = $(this),
                $response = $('#form-contato-footer-response');

            $response.fadeOut('fast');

            $.ajax({
                type: "POST",
                url: $('base').attr('href') + '/fale-conosco',
                data: {
                    nome: $('#nome_footer').val(),
                    email: $('#email_footer').val(),
                    telefone: $('#telefone_footer').val(),
                    mensagem: $('#mensagem_footer').val(),
                },
                success: function(data) {
                    $response.fadeOut().text(data.message).fadeIn('slow');
                    $form[0].reset();
                },
                error: function(data) {
                    $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
                },
                dataType: 'json'
            });
        });
    };

    App.anexoConta = function() {
        $('#input-conta').change(function(event) {
            event.preventDefault();

            if(!event.target.files.length) {
                $(this).parent().removeClass('active');
                $(this).next('span').html('ANEXAR CONTA DE LUZ');
                return;
            }

            $(this).parent().addClass('active');
            $(this).next('span').html(event.target.files[0].name);
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.envioContato();
        this.envioContatoFooter();
        this.anexoConta();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
