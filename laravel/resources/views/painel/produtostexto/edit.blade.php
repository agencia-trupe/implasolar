@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtostexto.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtostexto.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
