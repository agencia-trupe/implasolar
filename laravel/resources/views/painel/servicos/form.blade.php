@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_principal', 'Texto Principal') !!}
    {!! Form::textarea('texto_principal', null, ['class' => 'form-control ckeditor', 'data-editor' => 'servicosPrincipal']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_secundario', 'Texto Secundário') !!}
    {!! Form::textarea('texto_secundario', null, ['class' => 'form-control ckeditor', 'data-editor' => 'servicosSecundario']) !!}
</div>

<div class="form-group">
    {!! Form::label('rodape', 'Rodapé') !!}
    {!! Form::textarea('rodape', null, ['class' => 'form-control ckeditor', 'data-editor' => 'servicosRodape']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/servicos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
