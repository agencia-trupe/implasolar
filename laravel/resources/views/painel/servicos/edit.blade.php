@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Serviços
            <a href="{{ route('painel.financiamentos.index') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-usd" style="margin-right:10px;"></span>Financiamentos</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.servicos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.servicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
