@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('chamada_1_icone', 'Chamada 1 Ícone') !!}
            <img src="{{ url('assets/img/home/'.$registro->chamada_1_icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('chamada_1_icone', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_1_titulo', 'Chamada 1 Título') !!}
            {!! Form::text('chamada_1_titulo', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_1_texto', 'Chamada 1 Texto') !!}
            {!! Form::text('chamada_1_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('chamada_2_icone', 'Chamada 2 Ícone') !!}
            <img src="{{ url('assets/img/home/'.$registro->chamada_2_icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('chamada_2_icone', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_titulo', 'Chamada 2 Título') !!}
            {!! Form::text('chamada_2_titulo', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_texto', 'Chamada 2 Texto') !!}
            {!! Form::text('chamada_2_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('chamada_3_icone', 'Chamada 3 Ícone') !!}
            <img src="{{ url('assets/img/home/'.$registro->chamada_3_icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('chamada_3_icone', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_3_titulo', 'Chamada 3 Título') !!}
            {!! Form::text('chamada_3_titulo', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_3_texto', 'Chamada 3 Texto') !!}
            {!! Form::text('chamada_3_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('abertura_titulo', 'Abertura Título') !!}
    {!! Form::text('abertura_titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('abertura_texto', 'Abertura Texto') !!}
    {!! Form::textarea('abertura_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('codigo_video', 'Código Vídeo') !!}
    {!! Form::text('codigo_video', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('creditos_video', 'Créditos Vídeo') !!}
    {!! Form::textarea('creditos_video', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_secundario', 'Texto Secundário') !!}
            {!! Form::textarea('texto_secundario', null, ['class' => 'form-control ckeditor', 'data-editor' => 'homeSecundario']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_lateral', 'Imagem Lateral') !!}
            <img src="{{ url('assets/img/home/'.$registro->imagem_lateral) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_lateral', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
