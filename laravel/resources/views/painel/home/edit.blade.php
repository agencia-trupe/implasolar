@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Home
            <a href="{{ route('painel.utilizacoes.index') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-th" style="margin-right:10px;"></span>Utilizações</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.home.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.home.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
