<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.home*', Route::currentRouteName()) || str_is('painel.utilizacoes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.home.index') }}">Home</a>
    </li>
	<li @if(str_is('painel.empresa*', Route::currentRouteName()) || str_is('painel.o-que-oferecemos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.empresa.index') }}">Empresa</a>
	</li>
	<li @if(str_is('painel.servicos*', Route::currentRouteName()) || str_is('painel.financiamentos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.servicos.index') }}">Serviços</a>
	</li>
	<li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.produtos.index') }}">Produtos</a>
	</li>
    <li @if(str_is('painel.orcamentos-servicos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.orcamentos-servicos.index') }}">
            Orçamentos de Serviços
            @if($orcamentosServicosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $orcamentosServicosNaoLidos }}</span>
            @endif
        </a>
    </li>
    <li @if(str_is('painel.orcamentos-produtos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.orcamentos-produtos.index') }}">
            Orçamentos de Produtos
            @if($orcamentosProdutosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $orcamentosProdutosNaoLidos }}</span>
            @endif
        </a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
