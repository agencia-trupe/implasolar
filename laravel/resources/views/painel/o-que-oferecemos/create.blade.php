@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Empresa / O que oferecemos /</small> Adicionar Quadro</h2>
    </legend>

    {!! Form::open(['route' => 'painel.o-que-oferecemos.store', 'files' => true]) !!}

        @include('painel.o-que-oferecemos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
