@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Empresa / O que oferecemos /</small> Editar Quadro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.o-que-oferecemos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.o-que-oferecemos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
