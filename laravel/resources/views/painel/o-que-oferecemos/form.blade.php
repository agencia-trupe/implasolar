@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (opcional, só é exibida quando não houverem marcas cadastradas na galeria de imagens)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/o-que-oferecemos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.o-que-oferecemos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
