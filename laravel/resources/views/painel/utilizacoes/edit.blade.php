@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Utilizações /</small> Editar Utilização</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.utilizacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.utilizacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
