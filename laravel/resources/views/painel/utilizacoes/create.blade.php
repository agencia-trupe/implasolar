@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Utilizações /</small> Adicionar Utilização</h2>
    </legend>

    {!! Form::open(['route' => 'painel.utilizacoes.store', 'files' => true]) !!}

        @include('painel.utilizacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
