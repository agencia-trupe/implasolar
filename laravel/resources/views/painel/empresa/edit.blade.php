@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Empresa
            <a href="{{ route('painel.o-que-oferecemos.index') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-th" style="margin-right:10px;"></span>O que oferecemos</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.empresa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.empresa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
