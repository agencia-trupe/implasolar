@extends('frontend.common.template')

@section('content')

    <div class="home-areas">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                <div class="center">
                    <p>{!! $banner->texto !!}</p>
                </div>
            </div>
            @endforeach
        </div>

        <div class="chamadas">
            <div class="center">
                @foreach(range(1,3) as $i)
                <div class="chamada">
                    <img src="{{ asset('assets/img/home/'.$home->{'chamada_'.$i.'_icone'}) }}" alt="">
                    <h3>{{ $home->{'chamada_'.$i.'_titulo'} }}</h3>
                    <p>{{ $home->{'chamada_'.$i.'_texto'} }}</p>
                </div>
                @endforeach
            </div>
        </div>

        <div class="abertura">
            <div class="center">
                <h2>{{ $home->abertura_titulo }}</h2>
                {!! $home->abertura_texto !!}
            </div>
        </div>

        <div class="como-funciona">
            <div class="center">
                <h2>COMO FUNCIONA</h2>
                <div class="video">
                    <div class="embed-video">
                        <iframe src="https://youtube.com/embed/{{ $home->codigo_video }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                    </div>
                </div>
                <p class="creditos">{!! $home->creditos_video !!}</p>
            </div>
        </div>

        <div class="retorno">
            <div class="center">
                <h3>O RETORNO DO INVESTIMENTO SE DÁ EM MÉDIA EM 5 ANOS</h3>
                <div class="infografico">
                    <div>Conta de luz cada ano mais cara e com mais impostos</div>
                    <div>
                        <span>Geração de até 95% da energia consumida com a implantação de Painéis Solares</span>
                    </div>
                    <div>Retorno do investimento em média em 5 anos</div>
                </div>
            </div>
        </div>

        <div class="texto-secundario">
            <div class="center">
                <div class="texto">{!! $home->texto_secundario !!}</div>
                <img src="{{ asset('assets/img/home/'.$home->imagem_lateral) }}" alt="">
            </div>
        </div>

        <div class="utilizacoes">
            <div class="center">
                <h3>POSSIBILIDADES DE UTILIZAÇÃO DA ENERGIA SOLAR FOTOVOLTAICA</h3>
                <div class="utilizacoes-lista">
                    @foreach($utilizacoes as $utilizacao)
                    <div>
                        <img src="{{ asset('assets/img/utilizacoes/'.$utilizacao->imagem) }}" alt="">
                        <span>{{ $utilizacao->titulo }}</span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
