@extends('frontend.common.template')

@section('content')

    <div class="servicos">
        <div class="center">
            <div class="left">
                <div class="imagem">
                    <img src="{{ asset('assets/img/servicos/'.$servicos->imagem) }}" alt="">
                </div>

                <div class="orcamento">
                    <h3>SOLICITE UM ORÇAMENTO PARA<br>SUA RESIDÊNCIA OU EMPRESA</h3>
                    <p>Aproveite a fonte de energia mais limpa e abundante e ainda fique imune às constantes altas na tarifa de energia elétrica.</p>

                    @if(session('enviado'))
                    <p class="enviado">
                        Solicitação enviada com sucesso!<br>
                        Em breve entraremos em contato.
                    </p>
                    @else
                    <form action="{{ route('servicos.post') }}" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        @if($errors->any())
                        <p class="erro">
                            @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                            @endforeach
                        </p>
                        @endif

                        <input type="text" name="nome" id="nome" placeholder="nome" value="{{ old('nome') }}" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" value="{{ old('email') }}" required>
                        <input type="text" name="telefone" id="telefone" value="{{ old('telefone') }}" placeholder="telefone">
                        <textarea name="mensagem" id="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                        <div class="btn-conta">
                            <input type="file" name="conta_de_luz" id="input-conta">
                            <span>ANEXAR CONTA DE LUZ</span>
                        </div>
                        <input type="submit" value="ENVIAR">
                    </form>
                    @endif
                </div>

                <div class="financiamentos">
                    <h4>Confira as opções de financiamento</h4>
                    @foreach($financiamentos as $financiamento)
                    <img src="{{ asset('assets/img/financiamentos/'.$financiamento->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>

            <div class="right">
                <div class="texto-principal">
                    {!! $servicos->texto_principal !!}
                </div>

                <div class="texto-secundario">
                    {!! $servicos->texto_secundario !!}
                </div>

                <div class="rodape">
                    {!! $servicos->rodape !!}
                </div>
            </div>
        </div>
    </div>

@endsection
