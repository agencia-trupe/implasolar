    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('home') }}">home</a>
                <a href="{{ route('empresa') }}">empresa</a>
                <a href="{{ route('servicos') }}">serviços</a>
                <a href="{{ route('produtos') }}">produtos</a>
            </div>
            <div class="links">
                <a href="{{ route('orcamento') }}">solicite um orçamento</a>
                <a href="{{ route('contato') }}">fale conosco</a>
                <div class="social">
                    @foreach(['facebook', 'youtube', 'instagram'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}"></a>
                    @endif
                    @endforeach
                </div>
            </div>
            <form action="" id="form-contato-footer">
                <input type="text" name="nome" id="nome_footer" placeholder="nome" required>
                <input type="email" name="email" id="email_footer" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone_footer" placeholder="telefone">
                <input type="text" name="mensagem" id="mensagem_footer" placeholder="mensagem">
                <div id="form-contato-footer-response"></div>
                <input type="submit" value="ENVIAR">
            </form>
            <div class="info">
                <img src="{{ asset('assets/img/layout/logo.png') }}" alt="">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>
            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }}<br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
