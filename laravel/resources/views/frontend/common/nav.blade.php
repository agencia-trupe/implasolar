<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>
    home
</a>
<a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>
    empresa
</a>
<a href="{{ route('servicos') }}" @if(Tools::isActive('servicos')) class="active" @endif>
    serviços
</a>
<a href="{{ route('produtos') }}" @if(Tools::isActive('produtos')) class="active" @endif>
    produtos
</a>
<a href="{{ route('orcamento') }}" @if(Tools::isActive('orcamento')) class="active" @endif>
    solicite um orçamento
</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>
    fale conosco
</a>

<div class="social">
    @foreach(['facebook', 'youtube', 'instagram'] as $s)
    @if($contato->{$s})
    <a href="{{ $contato->{$s} }}" class="{{ $s }}"></a>
    @endif
    @endforeach
</div>
