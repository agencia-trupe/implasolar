@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="right">
                <p>{{ $contato->telefone }}</p>

                <form action="" id="form-contato">
                    <p>FALE CONOSCO</p>
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <div id="form-contato-response"></div>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>

            <div class="left">
                <div class="mapa">
                    {!! $contato->google_maps !!}
                </div>

                <p>{!! $contato->endereco !!}</p>
            </div>
        </div>
    </div>

@endsection
