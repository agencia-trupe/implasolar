@extends('frontend.common.template')

@section('content')

    <div class="empresa">
        <div class="textos">
            <div class="center">
                <div class="left">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_1) }}" alt="">
                    </div>

                    <div class="texto">
                        <h3>MISSÃO</h3>
                        <p>{{ $empresa->missao }}</p>

                        <h3>VISÃO</h3>
                        <p>{{ $empresa->visao }}</p>

                        <h3>VALORES</h3>
                        <p>{{ $empresa->valores }}</p>
                    </div>
                </div>

                <div class="right">
                    <div class="texto">
                        {!! $empresa->texto !!}
                    </div>

                    <div class="imagem">
                        <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_2) }}" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="quadros">
            <div class="center">
                <h2>O QUE OFERECEMOS:</h2>

                @foreach($quadros as $quadro)
                <div class="quadro">
                    <div class="texto">
                        <h4>{{ $quadro->titulo }}</h4>
                        <p>{{ $quadro->texto }}</p>
                    </div>

                    @if(count($quadro->imagens))
                        <div class="imagens">
                            @foreach($quadro->imagens as $imagem)
                            <img src="{{ asset('assets/img/o-que-oferecemos/imagens/'.$imagem->imagem) }}" alt="">
                            @endforeach
                        </div>
                    @elseif($quadro->imagem)
                        <img src="{{ asset('assets/img/o-que-oferecemos/'.$quadro->imagem) }}" alt="">
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
