@extends('frontend.common.template')

@section('content')

    <div class="produtos">
        <div class="center">
            <div class="texto">
                <h2>{{ $texto->titulo }}</h2>
                {!! $texto->texto !!}
            </div>

            <div class="orcamento">
                @if(session('enviado'))
                <p class="enviado">
                    Solicitação enviada com sucesso!<br>
                    Em breve entraremos em contato.
                </p>
                @else
                <form action="{{ route('produtos.post') }}" method="POST">
                    {!! csrf_field() !!}

                    <p>SOLICITE SEU ORÇAMENTO</p>
                    @if($errors->any())
                    <p class="erro">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </p>
                    @endif
                    <input type="text" name="nome" id="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" id="telefone" value="{{ old('telefone') }}" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem (informe a potência desejada ou o seu consumo em kWh)" required>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="ENVIAR">
                </form>
                @endif
            </div>

            <div class="produtos-lista">
                @foreach($produtos as $produto)
                <div class="produto">
                    <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                    <div class="texto">
                        <p>{!! $produto->titulo !!}</p>
                        @if($produto->arquivo)
                        <a href="{{ url('assets/produtos/arquivos/'.$produto->arquivo) }}" target="_blank">SAIBA MAIS</a>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
