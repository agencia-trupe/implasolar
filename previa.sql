-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: implasolar
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,0,'banner1_20170802161533.png','<strong>REDUZA SEUS GASTOS</strong><br />\r\nCOM ENERGIA EL&Eacute;TRICA COM PLACAS SOLARES<br />\r\n<strong>GERE ELETRICIDADE</strong><br />\r\nNA SUA PR&Oacute;PRIA CASA OU EMPRESA','2017-08-02 16:15:34','2017-08-02 16:15:34');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','11 3090 5976','Av. Paulista, 575&nbsp;&middot; 19&ordm; andar<br />\r\n01311-911&nbsp;&middot; S&atilde;o Paulo&nbsp;&middot; SP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.0119724944593!2d-46.650744984507405!3d-23.568013484679668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59bed9ec2aa5%3A0x88b4d1dcdbec1d16!2sCondom%C3%ADnio+Edif%C3%ADcio+Bar%C3%A3o+Ouro+Branco+-+Av.+Paulista%2C+575+-+Bela+Vista%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1501690361244\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>','#','#','#',NULL,'2017-08-02 16:12:48');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `missao` text COLLATE utf8_unicode_ci NOT NULL,
  `visao` text COLLATE utf8_unicode_ci NOT NULL,
  `valores` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in ante erat. Vivamus at odio felis. Aliquam pellentesque placerat urna, vel convallis libero malesuada a. Quisque ut suscipit purus, sit amet mattis purus. Aliquam blandit imperdiet enim consectetur accumsan.</p>\r\n\r\n<p>Proin sit amet leo dignissim, porttitor neque et, blandit nulla. Integer quis mauris sapien. Sed vitae urna nisi. Etiam in euismod arcu. Nulla ut dapibus nibh, sit amet condimentum mi. Curabitur rhoncus felis ac tincidunt iaculis. In sodales libero diam.</p>\r\n','Cras id ullamcorper enim, sit amet sodales tortor. Nulla tincidunt non dolor at luctus.','Cras id ullamcorper enim, sit amet sodales tortor. Nulla tincidunt non dolor at luctus.','Cras id ullamcorper enim, sit amet sodales tortor. Nulla tincidunt non dolor at luctus.','empresa1_20170802162232.jpg','empresa2_20170802162232.jpg',NULL,'2017-08-02 16:22:32');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `financiamentos`
--

DROP TABLE IF EXISTS `financiamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financiamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `financiamentos`
--

LOCK TABLES `financiamentos` WRITE;
/*!40000 ALTER TABLE `financiamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `financiamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home`
--

DROP TABLE IF EXISTS `home`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chamada_1_icone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_icone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_icone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `abertura_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abertura_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `codigo_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_secundario` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_lateral` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home`
--

LOCK TABLES `home` WRITE;
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` VALUES (1,'implantacao_20170802162105.png','Implantação do sistema de painéis solares','Processo simples, seguro e rápido com a Implasolar','conexao_20170802162106.png','Conexão do Sistema à rede da concessionária de energia elétrica','Sem complicações com a assessoria da Implasolar','reducao_20170802162106.png','Redução dos custos de energia elétrica em até 95%','Retorno do investimento se dá em média em 5 anos','PRODUZA ELETRICIDADE NO SEU PRÓPRIO TELHADO E ECONOMIZE NA CONTA DE LUZ','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in ante erat. Vivamus at odio felis. Aliquam pellentesque placerat urna, vel convallis libero malesuada a. Quisque ut suscipit purus, sit amet mattis purus.</p>\r\n\r\n<p>Aliquam blandit imperdiet enim consectetur accumsan. Proin sit amet leo dignissim, porttitor neque et, blandit nulla. Integer quis mauris sapien.&nbsp;</p>\r\n','xKxrkht7CpY','<p>Cras id ullamcorper enim, sit amet sodales tortor. Nulla tincidunt non dolor at luctus. Vivamus placerat tortor at ullamcorper venenatis. Sed porta tincidunt purus quis malesuada. Mauris dignissim neque nisi. Vivamus mi erat, laoreet et mauris et, ornare faucibus nunc. Morbi id velit porttitor, faucibus dui at, ultrices arcu. Suspendisse arcu felis, laoreet nec tincidunt aliquam, commodo nec ipsum.</p>\r\n\r\n<p>Donec mattis venenatis mauris. Suspendisse id leo nec lorem gravida pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec et urna tellus. In tempor elit in pulvinar semper. Proin mi magna, consectetur gravida luctus eget, viverra sit amet ex.</p>\r\n\r\n<p><a href=\"#\">MAIS INFORMA&Ccedil;&Otilde;ES COM A ANEEL&nbsp;&raquo;</a></p>\r\n','sistema_20170802162106.png',NULL,'2017-08-02 16:21:06');
/*!40000 ALTER TABLE `home` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2017_07_28_185708_create_produtos_table',1),('2017_07_28_190244_create_produtostexto_table',1),('2017_07_28_191008_create_financiamentos_table',1),('2017_07_28_191114_create_servicos_table',1),('2017_07_31_113436_create_banners_table',1),('2017_07_31_114351_create_home_table',1),('2017_07_31_114948_create_utilizacoes_table',1),('2017_07_31_115711_create_empresa_table',1),('2017_07_31_210655_create_o_que_oferecemos_table',1),('2017_08_01_160625_create_orcamentos_produtos_table',1),('2017_08_02_033240_create_orcamentos_servicos_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `o_que_oferecemos`
--

DROP TABLE IF EXISTS `o_que_oferecemos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `o_que_oferecemos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `o_que_oferecemos`
--

LOCK TABLES `o_que_oferecemos` WRITE;
/*!40000 ALTER TABLE `o_que_oferecemos` DISABLE KEYS */;
INSERT INTO `o_que_oferecemos` VALUES (1,0,'QUALIDADE NA INSTALAÇÃO E MANUTENÇÃO','Donec mattis venenatis mauris. Suspendisse id leo nec lorem gravida pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec et urna tellus. In tempor elit in pulvinar semper. Proin mi magna, consectetur gravida luctus eget, viverra sit amet ex.','qualidade-instalacao_20170802162306.png','2017-08-02 16:23:06','2017-08-02 16:23:06');
/*!40000 ALTER TABLE `o_que_oferecemos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `o_que_oferecemos_imagens`
--

DROP TABLE IF EXISTS `o_que_oferecemos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `o_que_oferecemos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quadro_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `o_que_oferecemos_imagens_quadro_id_foreign` (`quadro_id`),
  CONSTRAINT `o_que_oferecemos_imagens_quadro_id_foreign` FOREIGN KEY (`quadro_id`) REFERENCES `o_que_oferecemos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `o_que_oferecemos_imagens`
--

LOCK TABLES `o_que_oferecemos_imagens` WRITE;
/*!40000 ALTER TABLE `o_que_oferecemos_imagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `o_que_oferecemos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orcamentos_produtos`
--

DROP TABLE IF EXISTS `orcamentos_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orcamentos_produtos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orcamentos_produtos`
--

LOCK TABLES `orcamentos_produtos` WRITE;
/*!40000 ALTER TABLE `orcamentos_produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `orcamentos_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orcamentos_servicos`
--

DROP TABLE IF EXISTS `orcamentos_servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orcamentos_servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `conta_de_luz` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orcamentos_servicos`
--

LOCK TABLES `orcamentos_servicos` WRITE;
/*!40000 ALTER TABLE `orcamentos_servicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `orcamentos_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` VALUES (1,0,'Exemplo','gerador10_20170802162606.png','gerador10_20170802162606.png','2017-08-02 16:26:06','2017-08-02 16:26:06'),(2,0,'Exemplo','gerador10_20170802162616.png','gerador10_20170802162616.png','2017-08-02 16:26:16','2017-08-02 16:26:16');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtostexto`
--

DROP TABLE IF EXISTS `produtostexto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtostexto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtostexto`
--

LOCK TABLES `produtostexto` WRITE;
/*!40000 ALTER TABLE `produtostexto` DISABLE KEYS */;
INSERT INTO `produtostexto` VALUES (1,'GERADORES DE ENERGIA SOLAR FOTOVOLTAICA','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in ante erat. Vivamus at odio felis. Aliquam pellentesque placerat urna, vel convallis libero malesuada a. Quisque ut suscipit purus, sit amet mattis purus. Aliquam blandit imperdiet enim consectetur accumsan. Proin sit amet leo dignissim, porttitor neque et, blandit nulla. Integer quis mauris sapien. Sed vitae urna nisi. Etiam in euismod arcu. Nulla ut dapibus nibh, sit amet condimentum mi. Curabitur rhoncus felis ac tincidunt iaculis. In sodales libero diam.</p>\r\n',NULL,'2017-08-02 16:25:45');
/*!40000 ALTER TABLE `produtostexto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_principal` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_secundario` text COLLATE utf8_unicode_ci NOT NULL,
  `rodape` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,'servicos_20170802162521.jpg','<p>Realizamos o projeto e a instala&ccedil;&atilde;o de sistemas para gera&ccedil;&atilde;o de energia solar fotovoltaica conectados &agrave; rede de distribui&ccedil;&atilde;o.</p>\r\n\r\n<h3>ENTENDA COMO VOC&Ecirc; PODE TER ENERGIA SOLAR FOTOVOLTAICA NA SUA CASA OU EMPRESA:</h3>\r\n\r\n<ol>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n	<li>Donec in ante erat. Vivamus at odio felis.</li>\r\n	<li>Aliquam pellentesque placerat urna, vel convallis libero malesuada a.</li>\r\n	<li>Quisque ut suscipit purus, sit amet mattis purus.</li>\r\n	<li>Aliquam blandit imperdiet enim consectetur accumsan. Proin sit amet leo dignissim, porttitor neque et, blandit nulla.</li>\r\n	<li>Integer quis mauris sapien. Sed vitae urna nisi. Etiam in euismod arcu.</li>\r\n	<li>Nulla ut dapibus nibh, sit amet condimentum mi.</li>\r\n	<li>Curabitur rhoncus felis ac tincidunt iaculis. In sodales libero diam.</li>\r\n</ol>\r\n','<h3>O SOL QUE BRILHA PARA N&Oacute;S</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in ante erat. Vivamus at odio felis. Aliquam pellentesque placerat urna, vel convallis libero malesuada a.</p>\r\n','<p>Donec mattis venenatis mauris. Suspendisse id leo nec lorem gravida pellentesque. Pellentesque habitant morbi tristique senectus.</p>\r\n\r\n<p><em>Fonte: <a href=\"http://www.esalq.usp.br\">http://www.esalq.usp.br</a></em></p>\r\n',NULL,'2017-08-02 16:25:21');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$r16Bfp9ayJUCNzDYpzATHesmMSCg/.jWaadNisE/HsPttqoeyZul6',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilizacoes`
--

DROP TABLE IF EXISTS `utilizacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utilizacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilizacoes`
--

LOCK TABLES `utilizacoes` WRITE;
/*!40000 ALTER TABLE `utilizacoes` DISABLE KEYS */;
INSERT INTO `utilizacoes` VALUES (1,0,'Tecnologia para recarga de carros elétricos','regarga_20170802162133.jpg','2017-08-02 16:21:33','2017-08-02 16:21:33'),(2,0,'Instalações Residenciais','residenciais_20170802162151.jpg','2017-08-02 16:21:51','2017-08-02 16:21:51');
/*!40000 ALTER TABLE `utilizacoes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-02 16:26:51
